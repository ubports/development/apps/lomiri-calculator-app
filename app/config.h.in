/*
  Copyright (C) 2024 UBports Foundation.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

inline QString AppDir()
{
    static QString appDir = QString::fromLocal8Bit(getenv("APP_DIR"));
    return appDir;
}

inline QString AssetsDir()
{
    return AppDir() + QStringLiteral("@CMAKE_INSTALL_FULL_DATADIR@/lomiri-calculator-app");
}

inline QString I18nDirectory()
{
    return AppDir() + QStringLiteral("@CMAKE_INSTALL_FULL_LOCALEDIR@");
}

#endif // CONFIG_H
