#!/usr/bin/python3

import hashlib
import os
import sys
from pathlib import Path

organization = "calculator.ubports"
application = "calculator.ubports"
old_application = "com.ubuntu.calculator_reboot"

xdg_data_home = Path(
    os.environ.get("XDG_DATA_HOME", Path.home() / ".local" / "share")
)
db_file = (
    xdg_data_home / organization / "Databases" /
    f"{hashlib.md5(application.encode()).hexdigest()}.sqlite"
)
old_db_file = (
    db_file.parent /
    f"{hashlib.md5(old_application.encode()).hexdigest()}.sqlite"
)

if old_db_file.with_suffix(".ini").is_file() and not db_file.exists():
    with old_db_file.with_suffix(".ini").open() as old, \
         db_file.with_suffix(".ini").open(mode="w+") as new:
        for line in old:
            if line.strip() == f"Name={old_application}":
                line = f"Name={application}\n"
            new.write(line)
    old_db_file.with_suffix(".ini").unlink()
    old_db_file.rename(db_file)

if len(sys.argv) > 1:
    os.execvp(sys.argv[1], sys.argv[1:])
